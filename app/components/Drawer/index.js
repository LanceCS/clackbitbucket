import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import Typography from '@material-ui/core/Typography';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';

const drawerWidth = 220;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    primary: 'white',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: '#3F0E40',
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(3),
  },
  item: {
    color: 'white',
    fontSize: '2',
    display: 'flex',
  },
  list: {
    padding: 0,
    backgroundColor: '#3F0E40',
  },
  input: {
    '& label.Mui-focused': {
      color: 'white',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: 'white',
    },
  },
}));

export default function DrawerPerm() {
  const classes = useStyles();

  return (
    <Drawer
      className={classes.drawer}
      variant="permanent"
      classes={{
        paper: classes.drawerPaper,
      }}
      anchor="left"
    >
      <div
        style={{
          position: 'fixed',
          zIndex: 1,
          backgroundColor: '#3F0E40',
        }}
      >
        <List className={classes.list}>
          <div style={{ display: 'flex' }}>
            <ListItem className={classes.item} button>
              <Typography variant="h6">Stackhouse</Typography>
            </ListItem>
            <IconButton style={{ color: 'white' }}>
              <NotificationsNoneIcon style={{ color: 'white' }} />
            </IconButton>
          </div>
          <div style={{ padding: 10 }}>
            <TextField className={classes.input} label="My Label" />
          </div>
        </List>
      </div>

      <div className={classes.toolbar} />

      <Divider style={{ marginTop: '3rem' }} />

      <List>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
      </List>
      <Divider />
      <List>
        <ListItem className={classes.item} button>
          {'ASDASDASD '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
        <ListItem className={classes.item} button>
          {' '}
          Chat{' '}
        </ListItem>
      </List>
    </Drawer>
  );
}
