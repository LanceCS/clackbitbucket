import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import { fade, makeStyles } from '@material-ui/core/styles';
import CallOutlinedIcon from '@material-ui/icons/CallOutlined';
import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';
import Grid from '@material-ui/core/Grid';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import SettingsOutlinedIcon from '@material-ui/icons/SettingsOutlined';
import EmailOutlinedIcon from '@material-ui/icons/AlternateEmailOutlined';
import StarBorderOutlinedIcon from '@material-ui/icons/StarBorderOutlined';
import MoreVertOutlinedIcon from '@material-ui/icons/MoreVertOutlined';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    borderBottom: '0.5px solid rgba(220, 220, 220, 1)',
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 2,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
      overflow: 'inherit',
    },
  },
  search: {
    position: 'relative',
    border: '0.5px solid rgba(220, 220, 220, 1)',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 1),
      border: '1px solid grey',
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
}));

export default function NavContacts() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar
        style={{ backgroundColor: 'white', color: 'black', boxShadow: 'none' }}
        position="static"
      >
        <Toolbar style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div>
            <Grid container spacing={1}>
              <Grid item xs={12}>
                <Typography
                  color="initial"
                  className={classes.title}
                  variant="h6"
                  noWrap
                >
                  Nome Cognome
                </Typography>
              </Grid>
              <Grid item xs={3}>
                <Typography
                  color="initial"
                  className={classes.title}
                  variant="caption"
                  noWrap
                >
                  active
                </Typography>
              </Grid>
              <Grid item xs={5}>
                <Typography
                  variant="caption"
                  color="initial"
                  className={classes.title}
                  noWrap
                >
                  Nome Cognome
                </Typography>
              </Grid>
            </Grid>
          </div>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <IconButton>
              <CallOutlinedIcon />
            </IconButton>
            <IconButton>
              <InfoOutlinedIcon />
            </IconButton>
            <IconButton>
              <SettingsOutlinedIcon />
            </IconButton>

            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchOutlinedIcon />
              </div>
              <InputBase
                placeholder="Search…"
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
                inputProps={{ 'aria-label': 'search' }}
              />
            </div>
            <IconButton>
              <EmailOutlinedIcon />
            </IconButton>
            <IconButton>
              <StarBorderOutlinedIcon />
            </IconButton>
            <IconButton>
              <MoreVertOutlinedIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
