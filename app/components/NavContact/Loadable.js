/**
 *
 * Asynchronously loads the component for NavContact
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
