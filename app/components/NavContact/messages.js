/*
 * NavContact Messages
 *
 * This contains all the text for the NavContact component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.NavContact';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the NavContact component!',
  },
});
