/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';

import NotFoundPage from 'containers/NotFoundPage/Loadable';
import MainPage from 'containers/MainPage';
export default function App() {
  return (
    <Switch>
      <Route path="/main" component={MainPage} />
      <Route path="" component={NotFoundPage} />
    </Switch>
  );
}
